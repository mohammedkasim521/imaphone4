import React, {Component} from 'react';
import Index from './index';

class App extends Component {
    render() {
        return(
            <div>
                <Index />
            </div>
        )
    }
}
export default App;